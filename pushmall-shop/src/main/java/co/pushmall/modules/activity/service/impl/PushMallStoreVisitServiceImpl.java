package co.pushmall.modules.activity.service.impl;

import co.pushmall.modules.activity.domain.PushMallStoreVisit;
import co.pushmall.modules.activity.repository.PushMallStoreVisitRepository;
import co.pushmall.modules.activity.service.PushMallStoreVisitService;
import co.pushmall.modules.activity.service.dto.PushMallStoreVisitDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreVisitQueryCriteria;
import co.pushmall.modules.activity.service.mapper.PushMallStoreVisitMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-11-18
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStoreVisitServiceImpl implements PushMallStoreVisitService {

    private final PushMallStoreVisitRepository pushMallStoreVisitRepository;

    private final PushMallStoreVisitMapper pushMallStoreVisitMapper;

    public PushMallStoreVisitServiceImpl(PushMallStoreVisitRepository pushMallStoreVisitRepository, PushMallStoreVisitMapper pushMallStoreVisitMapper) {
        this.pushMallStoreVisitRepository = pushMallStoreVisitRepository;
        this.pushMallStoreVisitMapper = pushMallStoreVisitMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallStoreVisitQueryCriteria criteria, Pageable pageable) {
        Page<PushMallStoreVisit> page = pushMallStoreVisitRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallStoreVisitMapper::toDto));
    }

    @Override
    public List<PushMallStoreVisitDTO> queryAll(PushMallStoreVisitQueryCriteria criteria) {
        return pushMallStoreVisitMapper.toDto(pushMallStoreVisitRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStoreVisitDTO findById(Integer id) {
        Optional<PushMallStoreVisit> yxStoreVisit = pushMallStoreVisitRepository.findById(id);
        ValidationUtil.isNull(yxStoreVisit, "PushMallStoreVisit", "id", id);
        return pushMallStoreVisitMapper.toDto(yxStoreVisit.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStoreVisitDTO create(PushMallStoreVisit resources) {
        return pushMallStoreVisitMapper.toDto(pushMallStoreVisitRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStoreVisit resources) {
        Optional<PushMallStoreVisit> optionalPushMallStoreVisit = pushMallStoreVisitRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStoreVisit, "PushMallStoreVisit", "id", resources.getId());
        PushMallStoreVisit pushMallStoreVisit = optionalPushMallStoreVisit.get();
        pushMallStoreVisit.copy(resources);
        pushMallStoreVisitRepository.save(pushMallStoreVisit);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallStoreVisitRepository.deleteById(id);
    }
}
