package co.pushmall.modules.activity.service.impl;

import co.pushmall.modules.activity.domain.PushMallStoreCombination;
import co.pushmall.modules.activity.repository.PushMallStoreCombinationRepository;
import co.pushmall.modules.activity.repository.PushMallStorePinkRepository;
import co.pushmall.modules.activity.repository.PushMallStoreVisitRepository;
import co.pushmall.modules.activity.service.PushMallStoreCombinationService;
import co.pushmall.modules.activity.service.dto.PushMallStoreCombinationDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreCombinationQueryCriteria;
import co.pushmall.modules.activity.service.mapper.PushMallStoreCombinationMapper;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-11-18
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallStoreCombinationServiceImpl implements PushMallStoreCombinationService {

    private final PushMallStoreCombinationRepository pushMallStoreCombinationRepository;
    private final PushMallStorePinkRepository storePinkRepository;
    private final PushMallStoreVisitRepository storeVisitRepository;

    private final PushMallStoreCombinationMapper pushMallStoreCombinationMapper;

    public PushMallStoreCombinationServiceImpl(PushMallStoreCombinationRepository pushMallStoreCombinationRepository, PushMallStorePinkRepository storePinkRepository,
                                               PushMallStoreVisitRepository storeVisitRepository, PushMallStoreCombinationMapper pushMallStoreCombinationMapper) {
        this.pushMallStoreCombinationRepository = pushMallStoreCombinationRepository;
        this.storePinkRepository = storePinkRepository;
        this.storeVisitRepository = storeVisitRepository;
        this.pushMallStoreCombinationMapper = pushMallStoreCombinationMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallStoreCombinationQueryCriteria criteria, Pageable pageable) {
        criteria.setIsDel(0);
        Page<PushMallStoreCombination> page = pushMallStoreCombinationRepository
                .findAll((root, criteriaQuery, criteriaBuilder)
                        -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        List<PushMallStoreCombinationDTO> combinationDTOS = pushMallStoreCombinationMapper
                .toDto(page.getContent());
        for (PushMallStoreCombinationDTO combinationDTO : combinationDTOS) {
            //参与人数
            combinationDTO.setCountPeopleAll(storePinkRepository
                    .countByCid(combinationDTO.getId()));

            //成团人数
            combinationDTO.setCountPeoplePink(storePinkRepository.countByCidAndKId(combinationDTO.getId(),
                    0));
            //获取查看拼团产品人数
            combinationDTO.setCountPeopleBrowse(storeVisitRepository
                    .countByProductIdAndProductType(combinationDTO.getId(), "combination"));

            //System.out.println(combinationDTO);

        }
        Map<String, Object> map = new LinkedHashMap<>(2);
        map.put("content", combinationDTOS);
        map.put("totalElements", page.getTotalElements());

        return map;
        //return PageUtil.toPage(page.map(pushMallStoreCombinationMapper::toDto));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void onSale(Integer id, Integer status) {
        if (status == 1) {
            status = 0;
        } else {
            status = 1;
        }
        pushMallStoreCombinationRepository.updateOnsale(status, id);
    }

    @Override
    public List<PushMallStoreCombinationDTO> queryAll(PushMallStoreCombinationQueryCriteria criteria) {
        return pushMallStoreCombinationMapper.toDto(pushMallStoreCombinationRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallStoreCombinationDTO findById(Integer id) {
        Optional<PushMallStoreCombination> yxStoreCombination = pushMallStoreCombinationRepository.findById(id);
        ValidationUtil.isNull(yxStoreCombination, "PushMallStoreCombination", "id", id);
        return pushMallStoreCombinationMapper.toDto(yxStoreCombination.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallStoreCombinationDTO create(PushMallStoreCombination resources) {
        return pushMallStoreCombinationMapper.toDto(pushMallStoreCombinationRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallStoreCombination resources) {
        Optional<PushMallStoreCombination> optionalPushMallStoreCombination = pushMallStoreCombinationRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallStoreCombination, "PushMallStoreCombination", "id", resources.getId());
        PushMallStoreCombination yxStoreCombination = optionalPushMallStoreCombination.get();
        yxStoreCombination.copy(resources);
        pushMallStoreCombinationRepository.save(yxStoreCombination);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallStoreCombinationRepository.deleteById(id);
    }
}
