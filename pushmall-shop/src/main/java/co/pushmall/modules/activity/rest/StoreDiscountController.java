package co.pushmall.modules.activity.rest;

import cn.hutool.core.util.ObjectUtil;
import co.pushmall.aop.log.Log;
import co.pushmall.modules.activity.domain.PushMallStoreDiscount;
import co.pushmall.modules.activity.service.PushMallStoreDiscountService;
import co.pushmall.modules.activity.service.dto.PushMallStoreDiscountDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreDiscountQueryCriteria;
import co.pushmall.utils.OrderUtil;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@Api(tags = "商城:商品折扣管理")
@RestController
@RequestMapping("api")
public class StoreDiscountController {

    private final PushMallStoreDiscountService pushMallStoreDiscountService;

    public StoreDiscountController(PushMallStoreDiscountService pushMallStoreDiscountService) {
        this.pushMallStoreDiscountService = pushMallStoreDiscountService;
    }

    @Log("查询折扣")
    @ApiOperation(value = "查询折扣")
    @GetMapping(value = "/PmStoreDiscount")
    @PreAuthorize("@el.check('admin','PushMallStoreDiscount_ALL','PushMallStoreDiscount_SELECT')")
    public ResponseEntity getPushMallStoreDiscounts(PushMallStoreDiscountQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallStoreDiscountService.queryAll(criteria, pageable), HttpStatus.OK);
    }


    @Log("修改折扣")
    @ApiOperation(value = "修改折扣")
    @PutMapping(value = "/PmStoreDiscount")
    @PreAuthorize("@el.check('admin','PushMallStoreDiscount_ALL','PushMallStoreDiscount_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallStoreDiscount resources) {

        if (ObjectUtil.isNotNull(resources.getStartTimeDate())) {
            resources.setStartTime(OrderUtil.
                    dateToTimestamp(resources.getStartTimeDate()));
        }
        if (ObjectUtil.isNotNull(resources.getEndTimeDate())) {
            resources.setStopTime(OrderUtil.
                    dateToTimestamp(resources.getEndTimeDate()));
        }
        if (ObjectUtil.isNull(resources.getId())) {
            List<Map> list = JSON.parseArray(resources.getJsonStr().toString()).toJavaList(Map.class);
            resources.setAddTime(OrderUtil.getSecondTimestampTwo());
            for (Map map : list) {
                resources.setId((int) (Math.random() * (352324 + 1)));
                resources.setGrade(Integer.valueOf(map.get("grade").toString()));
                resources.setDiscount(Integer.valueOf(map.get("discount").toString()));
                PushMallStoreDiscountDTO pushMallStoreDiscountDTO = pushMallStoreDiscountService.create(resources);
            }
            return new ResponseEntity(HttpStatus.CREATED);
        } else {
            pushMallStoreDiscountService.update(resources);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @Log("删除折扣")
    @ApiOperation(value = "删除折扣")
    @DeleteMapping(value = "/PmStoreDiscount/{id}")
    @PreAuthorize("@el.check('admin','PushMallStoreDiscount_ALL','PushMallStoreDiscount_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id) {
        pushMallStoreDiscountService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    @Log("查询折扣")
    @ApiOperation(value = "查询折扣")
    @GetMapping(value = "/PmStoreDiscount/search/{productId}")
    @PreAuthorize("@el.check('admin','PushMallStoreDiscount_ALL','PushMallStoreDiscount_SELECT')")
    public ResponseEntity findByProductId(@PathVariable("productId") Integer productId) {
        return new ResponseEntity(pushMallStoreDiscountService.findByProductId(productId), HttpStatus.OK);
    }
}
