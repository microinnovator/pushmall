package co.pushmall.modules.activity.rest;

import cn.hutool.core.util.ObjectUtil;
import co.pushmall.aop.log.Log;
import co.pushmall.modules.activity.domain.PushMallStoreBargain;
import co.pushmall.modules.activity.service.PushMallStoreBargainService;
import co.pushmall.modules.activity.service.dto.PushMallStoreBargainQueryCriteria;
import co.pushmall.utils.OrderUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author pushmall
 * @date 2019-12-22
 */
@Api(tags = "商城:砍价管理")
@RestController
@RequestMapping("api")
public class StoreBargainController {

    private final PushMallStoreBargainService pushMallStoreBargainService;

    public StoreBargainController(PushMallStoreBargainService pushMallStoreBargainService) {
        this.pushMallStoreBargainService = pushMallStoreBargainService;
    }

    @Log("查询砍价")
    @ApiOperation(value = "查询砍价")
    @GetMapping(value = "/PmStoreBargain")
    @PreAuthorize("@el.check('admin','YXSTOREBARGAIN_ALL','YXSTOREBARGAIN_SELECT')")
    public ResponseEntity getPushMallStoreBargains(PushMallStoreBargainQueryCriteria criteria, Pageable pageable) {
        return new ResponseEntity(pushMallStoreBargainService.queryAll(criteria, pageable), HttpStatus.OK);
    }


    @Log("修改砍价")
    @ApiOperation(value = "修改砍价")
    @PutMapping(value = "/PmStoreBargain")
    @PreAuthorize("@el.check('admin','YXSTOREBARGAIN_ALL','YXSTOREBARGAIN_EDIT')")
    public ResponseEntity update(@Validated @RequestBody PushMallStoreBargain resources) {

        if (ObjectUtil.isNotNull(resources.getStartTimeDate())) {
            resources.setStartTime(OrderUtil.
                    dateToTimestamp(resources.getStartTimeDate()));
        }
        if (ObjectUtil.isNotNull(resources.getEndTimeDate())) {
            resources.setStopTime(OrderUtil.
                    dateToTimestamp(resources.getEndTimeDate()));
        }
        if (ObjectUtil.isNull(resources.getId())) {
            resources.setAddTime(OrderUtil.getSecondTimestampTwo());
            return new ResponseEntity(pushMallStoreBargainService.create(resources), HttpStatus.CREATED);
        } else {
            pushMallStoreBargainService.update(resources);
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }
    }

    @Log("删除砍价")
    @ApiOperation(value = "删除砍价")
    @DeleteMapping(value = "/PmStoreBargain/{id}")
    @PreAuthorize("@el.check('admin','YXSTOREBARGAIN_ALL','YXSTOREBARGAIN_DELETE')")
    public ResponseEntity delete(@PathVariable Integer id) {
        //if(StrUtil.isNotEmpty("22")) throw new BadRequestException("演示环境禁止操作");
        pushMallStoreBargainService.delete(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
