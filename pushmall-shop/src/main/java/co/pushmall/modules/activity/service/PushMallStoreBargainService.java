package co.pushmall.modules.activity.service;

import co.pushmall.modules.activity.domain.PushMallStoreBargain;
import co.pushmall.modules.activity.service.dto.PushMallStoreBargainDTO;
import co.pushmall.modules.activity.service.dto.PushMallStoreBargainQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-12-22
 */
//@CacheConfig(cacheNames = "yxStoreBargain")
public interface PushMallStoreBargainService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStoreBargainQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStoreBargainDTO> queryAll(PushMallStoreBargainQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStoreBargainDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStoreBargainDTO create(PushMallStoreBargain resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStoreBargain resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
