package co.pushmall.modules.activity.service.dto;

import co.pushmall.annotation.Query;
import lombok.Data;

/**
 * @author pushmall
 * @date 2019-11-18
 */
@Data
public class PushMallStorePinkQueryCriteria {
    @Query
    private Integer kId;
}
