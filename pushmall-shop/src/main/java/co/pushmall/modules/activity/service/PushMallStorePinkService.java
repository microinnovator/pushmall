package co.pushmall.modules.activity.service;

import co.pushmall.modules.activity.domain.PushMallStorePink;
import co.pushmall.modules.activity.service.dto.PushMallStorePinkDTO;
import co.pushmall.modules.activity.service.dto.PushMallStorePinkQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-11-18
 */
//@CacheConfig(cacheNames = "yxStorePink")
public interface PushMallStorePinkService {

    int countPeople(int id);

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStorePinkQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStorePinkDTO> queryAll(PushMallStorePinkQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStorePinkDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStorePinkDTO create(PushMallStorePink resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStorePink resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);
}
