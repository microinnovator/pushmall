package co.pushmall.modules.shop.service.impl;

import co.pushmall.modules.shop.domain.PushMallSystemConfig;
import co.pushmall.modules.shop.repository.PushMallSystemConfigRepository;
import co.pushmall.modules.shop.service.PushMallSystemConfigService;
import co.pushmall.modules.shop.service.dto.PushMallSystemConfigDTO;
import co.pushmall.modules.shop.service.dto.PushMallSystemConfigQueryCriteria;
import co.pushmall.modules.shop.service.mapper.PushMallSystemConfigMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-10-10
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallSystemConfigServiceImpl implements PushMallSystemConfigService {

    private final PushMallSystemConfigRepository pushMallSystemConfigRepository;

    private final PushMallSystemConfigMapper pushMallSystemConfigMapper;

    public PushMallSystemConfigServiceImpl(PushMallSystemConfigRepository pushMallSystemConfigRepository, PushMallSystemConfigMapper pushMallSystemConfigMapper) {
        this.pushMallSystemConfigRepository = pushMallSystemConfigRepository;
        this.pushMallSystemConfigMapper = pushMallSystemConfigMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallSystemConfigQueryCriteria criteria, Pageable pageable) {
        Page<PushMallSystemConfig> page = pushMallSystemConfigRepository
                .findAll((root, criteriaQuery, criteriaBuilder)
                        -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallSystemConfigMapper::toDto));
    }

    @Override
    public List<PushMallSystemConfigDTO> queryAll(PushMallSystemConfigQueryCriteria criteria) {
        return pushMallSystemConfigMapper.toDto(pushMallSystemConfigRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallSystemConfigDTO findById(Integer id) {
        Optional<PushMallSystemConfig> yxSystemConfig = pushMallSystemConfigRepository.findById(id);
        ValidationUtil.isNull(yxSystemConfig, "PushMallSystemConfig", "id", id);
        return pushMallSystemConfigMapper.toDto(yxSystemConfig.get());
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallSystemConfigDTO create(PushMallSystemConfig resources) {
        return pushMallSystemConfigMapper.toDto(pushMallSystemConfigRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallSystemConfig resources) {
        Optional<PushMallSystemConfig> optionalPushMallSystemConfig = pushMallSystemConfigRepository.findById(resources.getId());
        ValidationUtil.isNull(optionalPushMallSystemConfig, "PushMallSystemConfig", "id", resources.getId());
        PushMallSystemConfig pushMallSystemConfig = optionalPushMallSystemConfig.get();
        pushMallSystemConfig.copy(resources);
        pushMallSystemConfigRepository.save(pushMallSystemConfig);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer id) {
        pushMallSystemConfigRepository.deleteById(id);
    }

    @Override
    public PushMallSystemConfig findByKey(String str) {
        return pushMallSystemConfigRepository.findByMenuName(str);
    }
}
