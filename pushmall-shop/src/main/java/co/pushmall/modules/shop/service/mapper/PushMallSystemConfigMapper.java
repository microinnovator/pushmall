package co.pushmall.modules.shop.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.shop.domain.PushMallSystemConfig;
import co.pushmall.modules.shop.service.dto.PushMallSystemConfigDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-10-10
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallSystemConfigMapper extends EntityMapper<PushMallSystemConfigDTO, PushMallSystemConfig> {

}
