package co.pushmall.modules.shop.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.shop.domain.PushMallUser;
import co.pushmall.modules.shop.service.dto.PushMallUserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-10-06
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallUserMapper extends EntityMapper<PushMallUserDTO, PushMallUser> {

}
