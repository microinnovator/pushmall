package co.pushmall.modules.shop.service.mapper;

import co.pushmall.mapper.EntityMapper;
import co.pushmall.modules.shop.domain.PushMallSystemUserTask;
import co.pushmall.modules.shop.service.dto.PushMallSystemUserTaskDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * @author pushmall
 * @date 2019-12-04
 */
@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PushMallSystemUserTaskMapper extends EntityMapper<PushMallSystemUserTaskDTO, PushMallSystemUserTask> {

}
