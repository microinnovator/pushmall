package co.pushmall.modules.shop.repository;

import co.pushmall.modules.shop.domain.PushMallStoreProductAttrResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

/**
 * @author pushmall
 * @date 2019-10-13
 */
public interface PushMallStoreProductAttrResultRepository extends JpaRepository<PushMallStoreProductAttrResult, Integer>, JpaSpecificationExecutor {

    /**
     * findByProductId
     *
     * @param product_id
     * @return
     */
    PushMallStoreProductAttrResult findByProductId(Integer product_id);

    @Modifying
    @Transactional
    @Query(value = "delete from pushmall_store_product_attr_result where product_id =?1", nativeQuery = true)
    void deleteByProductId(Integer product_id);
}
