package co.pushmall.modules.shop.service.dto;

import lombok.Data;

import java.io.Serializable;


/**
 * @author pushmall
 * @date 2019-10-04
 */
@Data
public class PushMallStoreProductSmallDTO implements Serializable {

    // 商品id
    private Integer id;

    // 商品图片
    private String image;


    // 商品名称
    private String storeName;


}
