package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallStoreCategory;
import co.pushmall.modules.shop.service.dto.PushMallStoreCategoryDTO;
import co.pushmall.modules.shop.service.dto.PushMallStoreCategoryQueryCriteria;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-10-03
 */
//@CacheConfig(cacheNames = "yxStoreCategory")
public interface PushMallStoreCategoryService {

    /**
     * 导出数据
     *
     * @param queryAll 待导出的数据
     * @param response /
     * @throws IOException /
     */
    void download(List<PushMallStoreCategoryDTO> queryAll, HttpServletResponse response) throws IOException;


    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallStoreCategoryQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallStoreCategoryDTO> queryAll(PushMallStoreCategoryQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallStoreCategoryDTO findById(Integer id);

    PushMallStoreCategoryDTO findByName(String name);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallStoreCategoryDTO create(PushMallStoreCategory resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallStoreCategory resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);

    Object buildTree(List<PushMallStoreCategoryDTO> categoryDTOS);

}
