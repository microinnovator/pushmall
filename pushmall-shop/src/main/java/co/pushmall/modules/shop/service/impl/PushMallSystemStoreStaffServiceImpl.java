package co.pushmall.modules.shop.service.impl;

import co.pushmall.modules.shop.domain.PushMallSystemStoreStaff;
import co.pushmall.modules.shop.domain.PushMallUser;
import co.pushmall.modules.shop.repository.PushMallSystemStoreStaffRepository;
import co.pushmall.modules.shop.service.PushMallSystemStoreService;
import co.pushmall.modules.shop.service.PushMallSystemStoreStaffService;
import co.pushmall.modules.shop.service.PushMallUserService;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreDto;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreStaffDto;
import co.pushmall.modules.shop.service.dto.PushMallSystemStoreStaffQueryCriteria;
import co.pushmall.modules.shop.service.dto.PushMallUserDTO;
import co.pushmall.modules.shop.service.mapper.PushMallSystemStoreStaffMapper;
import co.pushmall.utils.FileUtil;
import co.pushmall.utils.OrderUtil;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2020-03-22
 */
@Service
//@CacheConfig(cacheNames = "yxSystemStoreStaff")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallSystemStoreStaffServiceImpl implements PushMallSystemStoreStaffService {

    private final PushMallSystemStoreStaffRepository pushMallSystemStoreStaffRepository;

    private final PushMallSystemStoreStaffMapper pushMallSystemStoreStaffMapper;

    private final PushMallSystemStoreService systemStoreService;

    private final PushMallUserService pushMallUserService;

    public PushMallSystemStoreStaffServiceImpl(PushMallSystemStoreStaffRepository pushMallSystemStoreStaffRepository,
                                               PushMallSystemStoreStaffMapper pushMallSystemStoreStaffMapper,
                                               PushMallSystemStoreService systemStoreService,
                                               PushMallUserService pushMallUserService) {
        this.pushMallSystemStoreStaffRepository = pushMallSystemStoreStaffRepository;
        this.pushMallSystemStoreStaffMapper = pushMallSystemStoreStaffMapper;
        this.systemStoreService = systemStoreService;
        this.pushMallUserService = pushMallUserService;
    }

    @Override
    //@Cacheable
    public Map<String, Object> queryAll(PushMallSystemStoreStaffQueryCriteria criteria, Pageable pageable) {
        Page<PushMallSystemStoreStaff> page = pushMallSystemStoreStaffRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(pushMallSystemStoreStaffMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<PushMallSystemStoreStaffDto> queryAll(PushMallSystemStoreStaffQueryCriteria criteria) {
        return pushMallSystemStoreStaffMapper.toDto(pushMallSystemStoreStaffRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public PushMallSystemStoreStaffDto findById(Integer id) {
        PushMallSystemStoreStaff pushMallSystemStoreStaff = pushMallSystemStoreStaffRepository.findById(id).orElseGet(PushMallSystemStoreStaff::new);
        ValidationUtil.isNull(pushMallSystemStoreStaff.getId(), "PushMallSystemStoreStaff", "id", id);
        return pushMallSystemStoreStaffMapper.toDto(pushMallSystemStoreStaff);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public PushMallSystemStoreStaffDto create(PushMallSystemStoreStaff resources) {
        PushMallUserDTO pushMallUserDTO = pushMallUserService.findById(resources.getUid());
        PushMallUser user = new PushMallUser();
        user.setUid(pushMallUserDTO.getUid());
        user.setSpreadUid(resources.getSpreadUid());
        user.setSpreadTime(Integer.valueOf(String.valueOf(System.currentTimeMillis() / 1000)));
        pushMallUserService.update(user);

        PushMallSystemStoreDto systemStoreDto = systemStoreService.findById(resources.getStoreId());
        resources.setStoreName(systemStoreDto.getName());
        resources.setAddTime(OrderUtil.getSecondTimestampTwo());
        return pushMallSystemStoreStaffMapper.toDto(pushMallSystemStoreStaffRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallSystemStoreStaff resources) {
        PushMallSystemStoreDto systemStoreDto = systemStoreService.findById(resources.getStoreId());
        resources.setStoreName(systemStoreDto.getName());
        PushMallSystemStoreStaff pushMallSystemStoreStaff = pushMallSystemStoreStaffRepository.findById(resources.getId()).orElseGet(PushMallSystemStoreStaff::new);
        ValidationUtil.isNull(pushMallSystemStoreStaff.getId(), "PushMallSystemStoreStaff", "id", resources.getId());
        pushMallSystemStoreStaff.copy(resources);
        pushMallSystemStoreStaffRepository.save(pushMallSystemStoreStaff);

        PushMallUserDTO pushMallUserDTO = pushMallUserService.findById(resources.getUid());
        PushMallUser user = new PushMallUser();
        user.setUid(pushMallUserDTO.getUid());
        user.setSpreadUid(resources.getSpreadUid());
        user.setSpreadTime(Integer.valueOf(String.valueOf(System.currentTimeMillis() / 1000)));
        pushMallUserService.update(user);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(Integer[] ids) {
        for (Integer id : ids) {
            pushMallSystemStoreStaffRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<PushMallSystemStoreStaffDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (PushMallSystemStoreStaffDto yxSystemStoreStaff : all) {
            Map<String, Object> map = new LinkedHashMap<>();
            map.put("微信用户id", yxSystemStoreStaff.getUid());
            map.put("店员头像", yxSystemStoreStaff.getAvatar());
            map.put("门店id", yxSystemStoreStaff.getStoreId());
            map.put("店员名称", yxSystemStoreStaff.getStaffName());
            map.put("手机号码", yxSystemStoreStaff.getPhone());
            map.put("核销开关", yxSystemStoreStaff.getVerifyStatus());
            map.put("状态", yxSystemStoreStaff.getStatus());
            map.put("添加时间", yxSystemStoreStaff.getAddTime());
            map.put("微信昵称", yxSystemStoreStaff.getNickname());
            map.put("所属门店", yxSystemStoreStaff.getStoreName());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }

    @Override
    public List<PushMallSystemStoreStaffDto> findByUid(Integer uid, Integer spreadUid) {
        return pushMallSystemStoreStaffMapper.toDto(pushMallSystemStoreStaffRepository.findByUid(uid, spreadUid));
    }
}
