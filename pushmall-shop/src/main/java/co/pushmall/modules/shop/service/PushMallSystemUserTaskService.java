package co.pushmall.modules.shop.service;

import co.pushmall.modules.shop.domain.PushMallSystemUserTask;
import co.pushmall.modules.shop.service.dto.PushMallSystemUserTaskDTO;
import co.pushmall.modules.shop.service.dto.PushMallSystemUserTaskQueryCriteria;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Map;

/**
 * @author pushmall
 * @date 2019-12-04
 */
//@CacheConfig(cacheNames = "yxSystemUserTask")
public interface PushMallSystemUserTaskService {

    /**
     * 查询数据分页
     *
     * @param criteria
     * @param pageable
     * @return
     */
    //@Cacheable
    Map<String, Object> queryAll(PushMallSystemUserTaskQueryCriteria criteria, Pageable pageable);

    /**
     * 查询所有数据不分页
     *
     * @param criteria
     * @return
     */
    //@Cacheable
    List<PushMallSystemUserTaskDTO> queryAll(PushMallSystemUserTaskQueryCriteria criteria);

    /**
     * 根据ID查询
     *
     * @param id
     * @return
     */
    //@Cacheable(key = "#p0")
    PushMallSystemUserTaskDTO findById(Integer id);

    /**
     * 创建
     *
     * @param resources
     * @return
     */
    //@CacheEvict(allEntries = true)
    PushMallSystemUserTaskDTO create(PushMallSystemUserTask resources);

    /**
     * 编辑
     *
     * @param resources
     */
    //@CacheEvict(allEntries = true)
    void update(PushMallSystemUserTask resources);

    /**
     * 删除
     *
     * @param id
     */
    //@CacheEvict(allEntries = true)
    void delete(Integer id);

    List<Map<String, Object>> getTaskType();
}
