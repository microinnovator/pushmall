package co.pushmall.constant;

/**
 * 商城统一常量
 *
 * @author pushmall
 * @since 2020-02-27
 */
public interface ShopConstants {

    /**
     * 订单自动取消时间（分钟）
     */
    long ORDER_OUTTIME_UNPAY = 30;
    /**
     * 订单自动收货时间（天）
     */
    long ORDER_OUTTIME_UNCONFIRM = 7;
    /**
     * redis订单未付款key
     */
    String REDIS_ORDER_OUTTIME_UNPAY = "order:unpay:";
    /**
     * redis订单收货key
     */
    String REDIS_ORDER_OUTTIME_UNCONFIRM = "order:unconfirm:";

    /**
     * redis拼团key
     */
    String REDIS_PINK_CANCEL_KEY = "pink:cancel:";

    /**
     * 微信支付service
     */
    String PUSHMALL_WEIXIN_PAY_SERVICE = "pushmall_weixin_pay_service";

    /**
     * 微信支付小程序service
     */
    String PUSHMALL_WEIXIN_MINI_PAY_SERVICE = "pushmall_weixin_mini_pay_service";

    /**
     * 微信支付app service
     */
    String PUSHMALL_WEIXIN_APP_PAY_SERVICE = "pushmall_weixin_app_pay_service";

    /**
     * 微信公众号service
     */
    String PUSHMALL_WEIXIN_MP_SERVICE = "pushmall_weixin_mp_service";


    /**
     * 商城默认密码
     */
    String PUSHMALL_DEFAULT_PWD = "123456";

    /**
     * 商城默认注册图片
     */
    String PUSHMALL_DEFAULT_AVATAR = "https://hr.dsc1688.cn/file/pic/avatar1_50-2020040312030211.png";

    /**
     * 腾讯地图地址解析
     */
    String QQ_MAP_URL = "https://apis.map.qq.com/ws/geocoder/v1/";

    /**
     * redis首页键
     */
    String PUSHMALL_REDIS_INDEX_KEY = "pushmall:index_data";

    /**
     * 充值方案
     */
    String PUSHMALL_RECHARGE_PRICE_WAYS = "pushmall_recharge_price_ways";
    /**
     * 首页banner
     */
    String PUSHMALL_HOME_BANNER = "pushmall_home_banner";
    /**
     * 首页菜单
     */
    String PUSHMALL_HOME_MENUS = "pushmall_home_menus";
    /**
     * 首页滚动新闻
     */
    String PUSHMALL_HOME_ROLL_NEWS = "pushmall_home_roll_news";
    /**
     * 热门搜索
     */
    String PUSHMALL_HOT_SEARCH = "pushmall_hot_search";
    /**
     * 个人中心菜单
     */
    String PUSHMALL_MY_MENUES = "pushmall_my_menus";
    /**
     * 秒杀时间段
     */
    String PUSHMALL_SECKILL_TIME = "pushmall_seckill_time";
    /**
     * 签到天数
     */
    String PUSHMALL_SIGN_DAY_NUM = "pushmall_sign_day_num";


}
