package co.pushmall.mp.service.impl;


import co.pushmall.mp.domain.PushMallWechatMenu;
import co.pushmall.mp.repository.PushMallWechatMenuRepository;
import co.pushmall.mp.service.PushMallWechatMenuService;
import co.pushmall.mp.service.dto.PushMallWechatMenuDTO;
import co.pushmall.mp.service.dto.PushMallWechatMenuQueryCriteria;
import co.pushmall.mp.service.mapper.PushMallWechatMenuMapper;
import co.pushmall.utils.PageUtil;
import co.pushmall.utils.QueryHelp;
import co.pushmall.utils.ValidationUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * @author pushmall
 * @date 2019-10-06
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class PushMallWechatMenuServiceImpl implements PushMallWechatMenuService {

    private final PushMallWechatMenuRepository PushMallWechatMenuRepository;

    private final PushMallWechatMenuMapper PushMallWechatMenuMapper;

    public PushMallWechatMenuServiceImpl(PushMallWechatMenuRepository PushMallWechatMenuRepository, PushMallWechatMenuMapper PushMallWechatMenuMapper) {
        this.PushMallWechatMenuRepository = PushMallWechatMenuRepository;
        this.PushMallWechatMenuMapper = PushMallWechatMenuMapper;
    }

    @Override
    public Map<String, Object> queryAll(PushMallWechatMenuQueryCriteria criteria, Pageable pageable) {
        Page<PushMallWechatMenu> page = PushMallWechatMenuRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder), pageable);
        return PageUtil.toPage(page.map(PushMallWechatMenuMapper::toDto));
    }

    @Override
    public List<PushMallWechatMenuDTO> queryAll(PushMallWechatMenuQueryCriteria criteria) {
        return PushMallWechatMenuMapper.toDto(PushMallWechatMenuRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root, criteria, criteriaBuilder)));
    }

    @Override
    public PushMallWechatMenuDTO findById(String key) {
        Optional<PushMallWechatMenu> PushMallWechatMenu = PushMallWechatMenuRepository.findById(key);
        ValidationUtil.isNull(PushMallWechatMenu, "PushMallWechatMenu", "key", key);
        return PushMallWechatMenuMapper.toDto(PushMallWechatMenu.get());
    }

    @Override
    public boolean isExist(String key) {
        Optional<PushMallWechatMenu> PushMallWechatMenu = PushMallWechatMenuRepository.findById(key);
        if (!PushMallWechatMenu.isPresent()) {
            return false;
        }
        return true;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PushMallWechatMenuDTO create(PushMallWechatMenu resources) {
        //resources.setKey(IdUtil.simpleUUID());
        return PushMallWechatMenuMapper.toDto(PushMallWechatMenuRepository.save(resources));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void update(PushMallWechatMenu resources) {
        Optional<PushMallWechatMenu> optionalPushMallWechatMenu = PushMallWechatMenuRepository.findById(resources.getKey());
        ValidationUtil.isNull(optionalPushMallWechatMenu, "PushMallWechatMenu", "id", resources.getKey());
        PushMallWechatMenu PushMallWechatMenu = optionalPushMallWechatMenu.get();
        PushMallWechatMenu.copy(resources);
        PushMallWechatMenuRepository.save(PushMallWechatMenu);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void delete(String key) {
        PushMallWechatMenuRepository.deleteById(key);
    }
}
